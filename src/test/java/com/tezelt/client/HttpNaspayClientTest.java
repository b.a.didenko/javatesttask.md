package com.tezelt.client;

import com.tezelt.client.client.HttpNaspayClient;
import com.tezelt.client.exception.EntityNotFoundException;
import com.tezelt.client.exception.InternalServiceException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.io.InputStream;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class HttpNaspayClientTest {

    private HttpNaspayClient naspayClient;

    @Mock
    private HttpClient httpClient;

    @Mock
    private HttpResponse response;

    @Mock
    private StatusLine statusLine;

    @Mock
    private HttpEntity httpEntity;

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        naspayClient = spy(new HttpNaspayClient(httpClient, "terminalKey", "terminalSecret"));
    }

    @ParameterizedTest
    @CsvSource({"authorized.json, 200, AUTHORIZED",
            "declined.json, 200, DECLINED",
            "timeout.json, 200, TIMEOUT",
            "completed.json, 200, COMPLETED"})
    public void shouldInvokeHttpClientWithGetRequestAndReturnStateResult(String fileName, int responseCode, String stage) throws Exception {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("token.json");
        InputStream inputStream2 = getClass().getClassLoader().getResourceAsStream(fileName);

        when(httpClient.execute(any(HttpGet.class))).thenReturn(response);
        when(response.getStatusLine()).thenReturn(statusLine);
        when(statusLine.getStatusCode()).thenReturn(responseCode);
        when(response.getEntity()).thenReturn(httpEntity);
        when(httpEntity.getContent()).thenReturn(inputStream, inputStream2);

        String result = naspayClient.getTransactionStatus(anyString());
        assertEquals(result, stage);
    }

    @ParameterizedTest
    @CsvSource({"502", "521"})
    public void shouldThrowInternalServiceExceptionWhenResourceReturnNon200StatusCode(int responseCode) throws Exception {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("token.json");

        when(httpClient.execute(any(HttpGet.class))).thenReturn(response);
        when(response.getStatusLine()).thenReturn(statusLine);
        when(statusLine.getStatusCode()).thenReturn(responseCode);
        when(response.getEntity()).thenReturn(httpEntity);
        when(httpEntity.getContent()).thenReturn(inputStream);

        assertThrows(InternalServiceException.class,
                () -> naspayClient.getTransactionStatus(anyString()));
    }

    @Test
    public void shouldThrowEntityNotFoundExceptionWhenResourceReturnNon200StatusCode() throws Exception {
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("token.json");

        when(httpClient.execute(any(HttpGet.class))).thenReturn(response);
        when(response.getStatusLine()).thenReturn(statusLine);
        when(statusLine.getStatusCode()).thenReturn(404);
        when(response.getEntity()).thenReturn(httpEntity);
        when(httpEntity.getContent()).thenReturn(inputStream);

        assertThrows(EntityNotFoundException.class,
                () -> naspayClient.getTransactionStatus(anyString()));
    }

    @Test
    public void shouldThrowInternalServiceExceptionWhenHttpClientThrowsAnyException() throws Exception {
        when(httpClient.execute(any(HttpGet.class)))
                .thenThrow(IOException.class);

        assertThrows(InternalServiceException.class,
                () -> naspayClient.getTransactionStatus(anyString()));
    }
}
