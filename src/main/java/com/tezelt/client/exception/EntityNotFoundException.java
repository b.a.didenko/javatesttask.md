package com.tezelt.client.exception;

public class EntityNotFoundException  extends Exception {

    public EntityNotFoundException(Throwable cause) {
        super(cause);
    }

    public EntityNotFoundException(String message) {
        super(message);
    }
}
