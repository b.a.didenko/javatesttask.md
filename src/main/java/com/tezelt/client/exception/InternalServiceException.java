package com.tezelt.client.exception;

public class InternalServiceException extends Exception {

    public InternalServiceException(Throwable cause) {
        super(cause);
    }

    public InternalServiceException(String message) {
        super(message);
    }
}

