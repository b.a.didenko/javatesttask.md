package com.tezelt.client.client;

import com.tezelt.client.exception.EntityNotFoundException;
import com.tezelt.client.exception.InternalServiceException;

public interface NaspayClient {

    String getTransactionStatus(String transactionId) throws InternalServiceException, EntityNotFoundException;
}
