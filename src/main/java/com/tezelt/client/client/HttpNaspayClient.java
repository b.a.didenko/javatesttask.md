package com.tezelt.client.client;


import com.tezelt.client.exception.EntityNotFoundException;
import com.tezelt.client.exception.InternalServiceException;
import com.tezelt.client.util.JsonUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.ServiceUnavailableRetryStrategy;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.protocol.HttpContext;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.Map;

/**
 * This is client class for get transaction status from https://demo.naspay.net/api/v1/transactions/.
 *
 * @author  Bohdan Didenko
 * @version 1.0
 * @since   2020-01-20
 */
@Slf4j
public class HttpNaspayClient implements NaspayClient {

    private HttpClient httpClient;
    private String terminalKey;
    private String terminalSecret;

    private static final long RETRY_INTERVAL = 1000;
    private static final int RETRY_COUNT = 3;
    private static final String ACCESS_TOKEN = "access_token";
    private static final String TOKEN_TYPE = "token_type";

    public HttpNaspayClient(String terminalKey, String terminalSecret) {
        this(createCustomClient(), terminalKey, terminalSecret);
    }

    public HttpNaspayClient(HttpClient httpClient, String terminalKey, String terminalSecret) {
        this.httpClient = httpClient;
        this.terminalKey = terminalKey;
        this.terminalSecret = terminalSecret;
    }

    /**
     * This method return Transaction status from https://demo.naspay.net/api/v1/transactions/.
     *
     * @param transactionId This is the transaction id parameter for get transaction state
     * @return String state - This is transaction state
     * @throws InternalServiceException if has any issue with connection to resource
     */
    @Override
    public String getTransactionStatus(String transactionId) throws InternalServiceException, EntityNotFoundException {
        String state = "";
        try {
            Map<String, String> map = getToken();
            URI uri = new URIBuilder(Endpoint.API_URL + transactionId)
                    .setParameter(ACCESS_TOKEN, map.get(ACCESS_TOKEN))
                    .setParameter(TOKEN_TYPE, map.get(TOKEN_TYPE))
                    .build();
            HttpGet request = new HttpGet(uri);
            request.setHeader(HttpHeaders.ACCEPT, ContentType.APPLICATION_JSON.getMimeType());
            state = executeRequest(request).get("state");
        } catch (URISyntaxException e) {
            log.error("URISyntaxException when try to create URIBuilder object", e);
            throw new InternalServiceException(e);
        }
        return state;
    }

    private static HttpClient createCustomClient() {
        return HttpClients.custom()
                .setServiceUnavailableRetryStrategy(new ServiceUnavailableRetryStrategy() {
                    @Override
                    public boolean retryRequest(
                            final HttpResponse response, final int executionCount, final HttpContext context) {
                        int statusCode = response.getStatusLine().getStatusCode();
                        return statusCode == 500 && executionCount < RETRY_COUNT;
                    }

                    @Override
                    public long getRetryInterval() {
                        return RETRY_INTERVAL;
                    }
                })
                .build();
    }

    private Map<String, String> getToken() throws InternalServiceException, EntityNotFoundException {
        HttpGet request = new HttpGet(Endpoint.API_TOKEN_URL);
        String authHeader = generateAuthHeader();
        request.setHeader(HttpHeaders.AUTHORIZATION, authHeader);
        return executeRequest(request);
    }

    private String generateAuthHeader() {
        String auth = terminalKey + ":" + terminalSecret;
        byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(StandardCharsets.ISO_8859_1));
        return "Basic " + new String(encodedAuth);
    }

    private Map<String, String> executeRequest(HttpRequestBase request) throws InternalServiceException, EntityNotFoundException {
        try {
            log.info("Sending request to: {}", request.getURI().getPath());
            HttpResponse response = httpClient.execute(request);
            int statusCode = response.getStatusLine().getStatusCode();
            InputStream responseBody = response.getEntity().getContent();

            if (statusCode != HttpStatus.SC_OK) {
                log.info("Http response code: {}", statusCode);
                throwException(statusCode, request.getURI().getPath());
            }

            return JsonUtils.fromJson(responseBody);
        } catch (IOException e) {
            log.error("Failed to execute request", e);
            throw new InternalServiceException(e);
        }
    }

    private void throwException(int statusCode, String URI) throws EntityNotFoundException, InternalServiceException {
        switch (statusCode) {
            case HttpStatus.SC_NOT_FOUND:
                throw new EntityNotFoundException("Status code - " + statusCode + " Entity is not found in resource - " + URI);
            case HttpStatus.SC_REQUEST_TIMEOUT:
            case HttpStatus.SC_UNSUPPORTED_MEDIA_TYPE:
            case HttpStatus.SC_INTERNAL_SERVER_ERROR:
            case HttpStatus.SC_REQUEST_TOO_LONG:
            default:
                throw new InternalServiceException("Status code - " + statusCode + " Internal Server Error in resource - " + URI);
        }
    }
}
