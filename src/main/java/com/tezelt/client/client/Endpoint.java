package com.tezelt.client.client;

public interface Endpoint {

    String API_TOKEN_URL= "https://demo.naspay.net/auth/token";

    String API_URL = "https://demo.naspay.net/api/v1/transactions/";
}
